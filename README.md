# Case study: Google Test and Google Benchmark 

Main takeaway:
 - Never use exponential fibonacci
 - Never use maps/sets in cpp. Always vectors! (cache misses)

This project illustrates how google test and google benchmark can be installed and used. The mock examples show how useful the libraries are.

You will be able to see 
 - how bad are sets and maps!! never use them. only use vectors to avoid cache misses
 - how sorting algorithms compare in time complexity (n vs logn), 
 - how massive the difference in fibonacci exponential vs linear is

## Outputs
- Bubble sort library
- Quick sort library
- Fibonacci library
- GTests for all
- Benchmarks for all

### How to run this
Clone the repo:
```
git clone path/to/git .
```

Make

```
mkdir build && cd build
cmake .. -DCMAKE_CXX_COMPILER=g++
make
```

### Results

```
cd bin
./benchmarking.tsk
``` 

See the output:

```
: ./benchmarking.tsk 
Run on (4 X 2900 MHz CPU s)
2017-11-04 21:04:14
***WARNING*** Library was built as DEBUG. Timings may be affected.
-------------------------------------------------------------
Benchmark                      Time           CPU Iterations
-------------------------------------------------------------
FibonnaciRecursive/2          15 ns         15 ns   45575587
FibonnaciRecursive/4          28 ns         28 ns   23709846
FibonnaciRecursive/8         158 ns        158 ns    4358492
FibonnaciRecursive/16       7331 ns       7310 ns      95204
FibonnaciRecursive/32   15960932 ns   15955953 ns         43
FibonnaciRecursive/40  765950526 ns  761047000 ns          1
FibonnaciIterative/2         644 ns        644 ns    1082770
FibonnaciIterative/4        1020 ns       1019 ns     697211
FibonnaciIterative/8        1457 ns       1454 ns     472144
FibonnaciIterative/16       1992 ns       1975 ns     346163
FibonnaciIterative/32       2866 ns       2843 ns     242365
FibonnaciIterative/40       3041 ns       3035 ns     225664
Quicksort/2                 8771 ns       8753 ns      78188
Quicksort/5                 9370 ns       9345 ns      75139
Quicksort/25               12298 ns      12283 ns      56155
Quicksort/125              28003 ns      27972 ns      24970
Quicksort/625             108150 ns     107693 ns       6332
Quicksort/3125            533475 ns     532056 ns       1242
Quicksort/10000          1740729 ns    1740469 ns        401
Bubblesort/2                8608 ns       8603 ns      77470
Bubblesort/5                9122 ns       9116 ns      76268
Bubblesort/25              13086 ns      13070 ns      50474
Bubblesort/125             60241 ns      59106 ns      11920
Bubblesort/625            830601 ns     828762 ns        815
Bubblesort/3125         19067189 ns   19048027 ns         37
Bubblesort/10000       187785271 ns  187027750 ns          4

```

Notice the asymptotic complexity of fibonacci methods and the bubble sort vs quicksort.

### The root of all evil - linked structures:
```
: ./bin/benchmarking.tsk --benchmark_filter=Search*
Run on (4 X 2900 MHz CPU s)
2017-11-04 23:25:30
-----------------------------------------------------------------
Benchmark                          Time           CPU Iterations
-----------------------------------------------------------------
SearchVector/2                  1152 ns       1148 ns     602908
SearchVector/5                  1156 ns       1152 ns     609130
SearchVector/25                 1162 ns       1157 ns     559785
SearchVector/125                1209 ns       1204 ns     574218
SearchVector/625                1167 ns       1163 ns     596995
SearchVector/3125               1207 ns       1198 ns     585113
SearchVector/10000              1308 ns       1290 ns     524510
SearchVectorSort/2              1156 ns       1152 ns     600704
SearchVectorSort/5              1189 ns       1185 ns     581168
SearchVectorSort/25             1483 ns       1477 ns     474757
SearchVectorSort/125            3879 ns       3876 ns     179303
SearchVectorSort/625           19498 ns      19481 ns      36405
SearchVectorSort/3125         115173 ns     115165 ns       5895
SearchVectorSort/10000        423204 ns     423198 ns       1648
SearchVectorIterate/2           1160 ns       1154 ns     609942
SearchVectorIterate/5           1166 ns       1160 ns     597897
SearchVectorIterate/25          1173 ns       1166 ns     594914
SearchVectorIterate/125         1305 ns       1299 ns     541092
SearchVectorIterate/625         1537 ns       1532 ns     453259
SearchVectorIterate/3125        3022 ns       3018 ns     230361
SearchVectorIterate/10000       6559 ns       6558 ns     106587
SearchSet/2                     1317 ns       1311 ns     531971
SearchSet/5                     1576 ns       1571 ns     443422
SearchSet/25                    3271 ns       3266 ns     215376
SearchSet/125                  11291 ns      11287 ns      60953
SearchSet/625                  50362 ns      50340 ns      13716
SearchSet/3125                240779 ns     240427 ns       2917
SearchSet/10000               775204 ns     774982 ns        868
```

Look at how badly a set performs on a real machine (Vs on the paper) compared to a vector. A set in theory has logn search time but that is simply ignoring how memory is layed out. In a cache oriented architecture, a vector does a lot better.

Even sorting the vector is better than searching in a set.

### Run the tests

```
: ./testingbubblesort.tsk 
[==========] Running 1 test from 1 test case.
[----------] Global test environment set-up.
[----------] 1 test from Bubblesort
[ RUN      ] Bubblesort.Works0
[       OK ] Bubblesort.Works0 (0 ms)
[----------] 1 test from Bubblesort (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test case ran. (0 ms total)
[  PASSED  ] 1 test.
```


