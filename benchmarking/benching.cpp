#include <benchmark/benchmark.h>
#include <iostream>
#include <random>
#include <set>
#include "fibonnaci.h"
#include "quicksort.h"
#include "bubblesort.h"

using namespace std;

void fillSet(multiset<int>& s, size_t n)
{
    
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(1.0, 30000.0);

    for(size_t i = 0; i <= n; ++i)
        s.insert(dist(mt));
}

void fillVector(vector<int>& vect)
{
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(1.0, 30000.0);

    for(auto& elem : vect)
        elem = dist(mt);
}

static void FibonnaciRecursive(benchmark::State& state)
{
    for (auto _ : state) 
    {
        unsigned int f = fib_recursive(state.range(0));
    }
    
}

static void FibonnaciIterative(benchmark::State& state)
{
    for (auto _ : state) 
    {
        unsigned int f = fib_iterative(state.range(0));
    } 
}

BENCHMARK(FibonnaciRecursive)->RangeMultiplier(2)->Range(2, 40);
BENCHMARK(FibonnaciIterative)->RangeMultiplier(2)->Range(2, 40);

static void Quicksort(benchmark::State& state)
{
    for (auto _ : state)
    {
        vector<int> numbers(state.range(0));
        fillVector(numbers);
        quicksort(numbers);
    }
}

static void Bubblesort(benchmark::State& state)
{
    for (auto _ : state)
    {
        vector<int> numbers(state.range(0));
        fillVector(numbers);
        bubblesort(numbers);
    }
}

BENCHMARK(Quicksort)->RangeMultiplier(5)->Range(2, 10000);
BENCHMARK(Bubblesort)->RangeMultiplier(5)->Range(2, 10000);

class Search : public benchmark::Fixture 
{
    void SetUp(const ::benchmark::State& state) 
    {
        size_t num = state.range(0);
        numbers_v.reserve(num);
        fillVector(numbers_v);
        fillSet(numbers_s, num);
    }

    void TearDown(const ::benchmark::State&) {
        numbers_v.clear();
        numbers_s.clear();
    }
            
    multiset<int> numbers_s;
    vector<int> numbers_v;

};


static void SearchVector(benchmark::State& state)
{
    for (auto _ : state)
    {
        state.PauseTiming();
        vector<int> numbers(state.range(0));
        fillVector(numbers);
        state.ResumeTiming();

        std::find(numbers.begin(), numbers.end(), 12345);
    }
}

static void SearchVectorIterate(benchmark::State& state)
{
    for (auto _ : state)
    {
        state.PauseTiming();
        vector<int> numbers(state.range(0));
        fillVector(numbers);
        state.ResumeTiming();
        for (size_t i = 0; i < numbers.size(); ++i)
        {
            if (numbers[i] == 12345)
                break;
        }
    }
}
static void SearchVectorSort(benchmark::State& state)
{
    for (auto _ : state)
    {
        state.PauseTiming();
        vector<int> numbers(state.range(0));
        fillVector(numbers);
        state.ResumeTiming();

        sort(numbers.begin(), numbers.end());
        binary_search(numbers.begin(), numbers.end(), 12345);
    }
}
static void SearchSet(benchmark::State& state)
{
    for (auto _ : state)
    {   
        state.PauseTiming();
        multiset<int> numbers;
        fillSet(numbers, state.range(0));
        state.ResumeTiming();

        numbers.find(12345);
    }
}



BENCHMARK(SearchVector)->RangeMultiplier(5)->Range(2, 10000);
BENCHMARK(SearchVectorSort)->RangeMultiplier(5)->Range(2, 10000);
BENCHMARK(SearchVectorIterate)->RangeMultiplier(5)->Range(2, 10000);
BENCHMARK(SearchSet)->RangeMultiplier(5)->Range(2, 10000);

BENCHMARK_MAIN();
